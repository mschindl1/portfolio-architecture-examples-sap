= Data center to edge
 Ishu Verma  @ishuverma, Francesco Rossi
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

_Some details will differ based on the requirements of a specific implementation but all portfolio architectures generalize one or more successful deployments of a use case._

*Use case:* Bringing computing closer to the edge by monitoring for potential issues with gas pipeline (edge).

*Background:* Energy (utility) infrastructure companies operate across vast geographical area that connects the upstream drilling operations with downstream fuel processing and delivery to customers. These companies need to monitor the condition of pipeline and other infrastructure for
operational safety and optimization.

== Solution overview

This architecture covers the use case around data center to edge. 

====
*Data center to edge*

. Modernize operations from data center to edge
. Real time access to IoT data
. Dynamic energy management in response to customer demand
====


--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/datacenter-to-edge-marketing-slide.png[alt="High level view of solution", width=700]
--

== Logical diagram
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/datacenter-to-edge-ld.png[alt="Conceptual view of solution components deployed at various locations", width=700]
--

== The technology


The following technology was chosen for this solution:

====
https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift*] is an enterprise-ready Kubernetes container platform built for an open hybrid cloud strategy.
It provides a consistent application platform to manage hybrid cloud, multicloud, and edge deployments.

https://www.redhat.com/en/products/middleware?intcmp=7013a00000318EWAAY[*Red Hat Application Services*] helps organizations use the cloud delivery model and simplify continuous delivery of
applications, the cloud-native way. Built on proven open source technologies, it also provides development teams
multiple modernization options to enable a smooth transition to the cloud for existing applications.

https://catalog.redhat.com/software/operators/detail/5ef20efd46bc301a95a1e9a4?intcmp=7013a00000318EWAAY[*Red Hat AMQ Streams*] data streaming platform with high throughput and low latency. Streams sensor data to corresponding microservices to automated diagnosis.

https://www.redhat.com/en/technologies/management/advanced-cluster-management?intcmp=7013a00000318EWAAY[*Red Hat Advanced Cluster Management*] for Kubernetes controls clusters and applications from a single console, with
built-in security policies. Extend the value of Red Hat OpenShift by deploying apps, managing multiple clusters, and
enforcing policies across multiple clusters at scale.

https://www.redhat.com/en/technologies/cloud-computing/quay?intcmp=7013a00000318EWAAY[*Red Hat Quay*] is a private container registry that stores, builds, and deploys container images. It analyzes your
images for security vulnerabilities, identifying potential issues that can help you mitigate security risks.

https://www.redhat.com/en/technologies/cloud-computing/openshift-data-foundation?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Data Foundations*] is software-defined storage for containers. Engineered as the data and storage
services platform for Red Hat OpenShift, Red Hat OpenShift Data Foundation helps teams develop and deploy applications
quickly and efficiently across clouds.

https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux?intcmp=7013a00000318EWAAY[*Red Hat Enterprise Linux*] is the world’s leading enterprise Linux platform. It’s an open source operating system
(OS). It’s the foundation from which you can scale existing apps—and roll out emerging technologies—across bare-metal,
virtual, container, and all types of cloud environments.
====


== Architectures

=== Data center to edge
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/datacenter-to-edge-data-sd.png[alt="Data interaction of centralized and edge components", width=700]
--

At the edge locations, the telemetry data from sensors is transmitted Edge Microservice application for protocol conversion/normalization and then forwarded to Red Hat AMQ message broker, which then routes it to Message Gateway which is a SpringBoot application for sending this data to the core data center.

At the core data center, the edge data event stream is received by
Red Hat AMQ Streams and sent to Core Microservices for further processing. The container and non-container storage components provide long term persistent storage. The data is stored into SQL and no-SQL databases for further access.


=== Data center to edge management
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/datacenter-to-edge-management-sd.png[alt="Cluster and applicationnlifecycle management using ACM and DevOps", width=700]
--

In order to centrally manage the geographically dispersed edge clusters, a consistent approach is needed. Red Hat ACM provides cluster lifecycle management for edge and centralized clusters. For DevOps, the OpenShift Pipelines enables the CI/CD workflow with the containerized applications delivered to Red Hat Quay image registry in the cloud. The application monitoring provided by Dynatrace enables the application optimization across edge and core sites.

== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/datacenter-to-edge.drawio[[Open Diagrams]]
--

== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/datacenter-to-edge.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].
